import * as firebase from 'firebase';

const prodConfig = {
    apiKey: "AIzaSyAT1eQUQNfsB2Rk-0uZBlTh6FJHFbU_Vgk",
    authDomain: "barapp-67e3e.firebaseapp.com",
    databaseURL: "https://barapp-67e3e.firebaseio.com",
    projectId: "barapp-67e3e",
    storageBucket: "barapp-67e3e.appspot.com",
    messagingSenderId: "58373907251"
  };
  
  const devConfig = {
    apiKey: "AIzaSyAT1eQUQNfsB2Rk-0uZBlTh6FJHFbU_Vgk",
    authDomain: "barapp-67e3e.firebaseapp.com",
    databaseURL: "https://barapp-67e3e.firebaseio.com",
    projectId: "barapp-67e3e",
    storageBucket: "barapp-67e3e.appspot.com",
    messagingSenderId: "58373907251"
  };
  
  const config = process.env.NODE_ENV === 'production'
    ? prodConfig
    : devConfig;
  
  if (!firebase.apps.length) {
    firebase.initializeApp(config);
  }

  const db = firebase.database();
  //const auth = firebase.auth();
  
  export {
    db, 
    //auth,
  };