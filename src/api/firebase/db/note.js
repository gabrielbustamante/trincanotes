import { db } from '../firebase';

export const doCreateNote = (title, description) =>{
  var newNoteKey = db.ref().child('notes').push().key;
  var x = 10;
  var y = 20;
  var zIndex = 10000;
  var tempDate = new Date();
  var createDate = tempDate.getDate() + '-' + (tempDate.getMonth()+1) + '-' + tempDate.getFullYear() +' '+ tempDate.getHours()+':'+ tempDate.getMinutes()+':'+ tempDate.getSeconds(); 
  return db.ref(`notes/${newNoteKey}`).set({
    title,
    description,
    createDate,
    x,
    y,
    zIndex
  });
}

  export const onceGetNotes = () =>
  db.ref('notes').once('value');

  export const onGetNotes = () =>
  {
    return new Promise((resolve, reject) => {
      db.ref('notes').on('value', function(data) {
        resolve(data);
      });
      
    });
  }