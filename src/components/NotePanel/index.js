import React from "react";
import Note from "../Note";

const NotePanel = ({items}) =>(
    <div>
        {items.map(function(note){
            return(
                <Note title={note.title} description={note.description} />
            );
        })}

    </div>

);

export default NotePanel;