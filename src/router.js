import React from "react";
import { Route, Switch, Router } from "react-router";
import { Provider } from "react-redux";
import createHistory from "history/createBrowserHistory";
import NoteContainer from "./containers/NoteContainer";
export const history = createHistory();

const RouterComponent = ({ store }) => {
  return (
    <Provider store={store}>
      <Router history={history}>
        <Switch>
          <Route exact path="/Notes" component={NoteContainer} />       
        </Switch>
      </Router>
    </Provider>
  );
};

export default RouterComponent;