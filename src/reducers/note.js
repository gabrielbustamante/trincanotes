import {
    GET_NOTES
} from "../actions/types";

import {Map} from "immutable"

const INITAL_STATE = Map({
    noteList:[]
})

export default (state =INITAL_STATE, action)=>{
    switch(action.type){
        case GET_NOTES:{
            console.log(action.payload.items)
            return state.setIn(["noteList"], action.payload.items)
        }
        default:
            return state;
    }
}