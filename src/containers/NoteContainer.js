import React , {Component} from "react";
import NotePanel from "../components/NotePanel";
import {getNotes} from "../actions/note";
import {connect } from "react-redux";



class NoteContainer extends Component{
    constructor(props){
        super(props);
        document.title="Notes";
    }

    componentDidMount(){
        const {getNotes} = this.props;
        getNotes();
    }


    render(){
        return(
            <div>Over clock react
                <NotePanel items={this.props.noteList}/>
            </div>
        );
    }
    
    
}

const mapStateToProps = state =>{
    return {
        noteList: state.get("note").get("noteList")
    };
;}

const mapDispatchToProps ={
    getNotes
};


export default connect(mapStateToProps,mapDispatchToProps)(NoteContainer);