import { combineReducers } from "redux-immutable";
import { routerReducer } from "react-router-redux";
import note from "./note.js";


const appReducer = combineReducers({
  note,
  router: routerReducer
});

const rootReducer = (state, action) => {
  return appReducer(state, action);
};

export default rootReducer;
