import React from "react";
import "./index.css"
import Draggable from "react-draggable";

const Note = ({title, description}) =>(
    <Draggable>

    <div className="post-it">
        <label>{title}</label>
        <label>{description}</label>

    </div>
    </Draggable>


);

export default Note;