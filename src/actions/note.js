import {
    GET_NOTES
} from "./types"

import {note} from  "../api/firebase"
import { db } from '../api/firebase/firebase';

export const getNotes = () => dispatch =>{
    // note.onGetNotes().then(
    //     (data) =>{
    //         dispatch(getNotesSuccessSnapshot(data))
    //     }
    // )
    return new Promise((resolve,reject) =>{
        db.ref('notes').on('value', function(data) {
            dispatch(getNotesSuccessSnapshot(data));
          });
        resolve();
    });

};

export const getNotesSuccessSnapshot = (data) => dispatch =>{
    var returnArray = [];
    data.forEach(function(childData){
        var item = childData.val();
        returnArray.push(item);
    });
    dispatch(getNotesSuccess({items: returnArray}))
};

export const getNotesSuccess = items =>({
    type: GET_NOTES,
    payload: items
});